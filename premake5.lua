workspace "netvis"
    if os.host() == "windows" then
        architecture "x64"
    end

    configurations {
        "debug",
        "release"
    }

include "netcom"
include "addressclass"
include "subnet"

