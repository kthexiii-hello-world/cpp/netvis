#include <array>
#include <chrono>
#include <cstdint>
#include <iostream>
#include <sstream>
#include <string>
#include <thread>

#include "netcom.h"

//
// IPv4
// 00000000.00000000.00000000.00000000
//
// Class A Address
//
// 00000000
// ↑
// Zero first bit is indicating that it is a class A address
//
// Which means that the address range can be between 1 to 127
// 00000001 - 01111111
//        1 - 127
//
// Class A addresses only include IP starting from 1.x.x.x to 126.x.x.x only.
// IP range 127.x.x.x is reserved for loopback IP address.
//
// Default subnet mask is 255.0.0.0 which implies that Class A addressing can
// have 126 networks (2^7-2) and 2^24-2 hosts
//
// 0NNNNNNN.HHHHHHHH.HHHHHHHH.HHHHHHHH
//
// Where N is the network ID and H is the host address
//
// Class B Address
//
// 10000000
// ↑↑
// One, Zero on first two bits is indicating that is a class B address
//
// Which have the address range between 128 - 191
// 10000001 - 10111111
//      128 - 191
//
// IP address range 128.0.x.x to 192.255.x.x. Default subnet mask for Class B
// is 255.255.x.x.
//
// Network IDs/Addresses 2^14 and 2^16-2 host addresses.
//
// 10NNNNNN.NNNNNNNN.HHHHHHHH.HHHHHHHH
//
// Class C Address
//
// 11000000
// ↑↑↑
// One, One, Zero on first three bit indicates that it is a class C address
//
// Which have the address range between
// 11000000 - 11011111
//      192 - 223
//
// IP address range from 192.0.0.x to 223.255.255.x. Default subnet mask for
// Class C is 255.255.255.x.
//
// Network IDs/Addresses 2^21 and 2^8-2 host addresses.
//
// 110NNNNN.NNNNNNNN.NNNNNNNN.HHHHHHHH
//
// Class D Address
//
// 11100000
// ↑↑↑↑
// One One One Zero on first four bit indicates that is a class D address
//
// Which have the address range between
// 11100000 - 11101111
//      224 - 239
//
// Class D has IP address range from 224.0.0.0 to 239.255.255.255. Class D is
// reserved for Multicasting. In multicasting data is not destined for a
// particular host, that is why there is no need to extract host address from
// the IP address, and Class D does not have any subnet mask.
//
// Class E
//
// 240 - 255.255.255.254
//
// Experimental
//
// source: https://www.tutorialspoint.com/ipv4/ipv4_address_classes.htm
//

constexpr uint32_t CLASSA_ADDRESS_MASK = 0x80000000;  // 10000000...
constexpr uint32_t CLASSB_ADDRESS_MASK = 0xC0000000;  // 11000000...
constexpr uint32_t CLASSC_ADDRESS_MASK = 0xE0000000;  // 11100000...

constexpr uint32_t CLASSA_LEAD = 0b00000000000000000000000000000000;
constexpr uint32_t CLASSB_LEAD = 0b10000000000000000000000000000000;
constexpr uint32_t CLASSC_LEAD = 0b11000000000000000000000000000000;

constexpr uint32_t CLASS_A_SUBMASK = 0xFF000000;
constexpr uint32_t CLASS_B_SUBMASK = 0xFFFF0000;
constexpr uint32_t CLASS_C_SUBMASK = 0xFFFFFF00;

auto main(int argc, char const* argv[]) -> int {
    std::cout << "\033[H\033[2J\n";  // Clears the screen

    std::cout << " IP Address Class\n";
    std::cout << " -----------------\n";

    std::cout << "\n";

    using namespace std::chrono_literals;

    const std::string ipv4Text = "129.168.40.55";
    std::cout << " IP Address:   ";
    uint32_t ipv4 = IPv4::TextToU32(ipv4Text);
    std::cout << IPv4::FormatDec(ipv4) << "\n\n";

    std::cout << " Checking address range";
    for(int32_t i = 0; i < 6; i++) {
        std::cout << '.' << std::flush;
        std::this_thread::sleep_for(100ms);
    }
    std::cout << "\n\n";

    std::cout << " Address in \n";
    std::cout << " Binary: ";
    std::cout << IPv4::FormatBin(ipv4) << "\n\n";
    std::this_thread::sleep_for(100ms);

    enum class IP_CLASS_SUBMASK : uint8_t {
        None, A, B, C
    };

    IP_CLASS_SUBMASK submask;

    uint32_t testClassAIPv4 = ipv4 & CLASSA_ADDRESS_MASK;
    uint32_t testClassBIPv4 = ipv4 & CLASSB_ADDRESS_MASK;
    uint32_t testClassCIPv4 = ipv4 & CLASSC_ADDRESS_MASK;

    std::cout << " The address class is: \n\n";
    std::cout << "   Class ";

    if (testClassAIPv4 == CLASSA_LEAD) {
        std::cout << "A";
        submask = IP_CLASS_SUBMASK::A;
    } else if(testClassBIPv4 == CLASSB_LEAD) {
        std::cout << "B";
        submask = IP_CLASS_SUBMASK::B;
    } else if(testClassCIPv4 == CLASSC_LEAD) {
        std::cout << "C";
        submask = IP_CLASS_SUBMASK::C;
    } else {
        std::cout << "D or E";
        submask = IP_CLASS_SUBMASK::None;
    }

    std::cout << "\n\n";

    std::cout << " Subnet mask: ";
    if (submask == IP_CLASS_SUBMASK::A) {
        std::cout << IPv4::FormatDec(CLASS_A_SUBMASK);
    } else if (submask == IP_CLASS_SUBMASK::B) {
        std::cout << IPv4::FormatDec(CLASS_B_SUBMASK);
    } else if (submask == IP_CLASS_SUBMASK::C) {
        std::cout << IPv4::FormatDec(CLASS_C_SUBMASK);
    } else {
        std::cout << " None";
    }
    std::cout << "\n\n";

    // std::cout << " Subnet Mask:  ";
    // std::cout << IPv4FormatDec(classCMask) << '\n';
    // std::cout << " Network ID:   ";
    // std::cout << IPv4FormatDec(ipv4 & classCMask) << '\n';
    // std::cout << " Host Address: ";
    // std::cout << IPv4FormatDec(ipv4 & ~classCMask) << '\n';

    // std::cout << "\033[?25l";

    // for (int32_t i = 0; i < 32; i++) {
    //     // std::cout << moveLeft100;
    //     std::cout << "\u001b[1D" << " ";
    //     std::cout << "↓" << std::flush;
    //     std::this_thread::sleep_for(100ms);
    // }

    // std::cout << "\033[?25h";
    // std::cout << "\n";

    return 0;
}

