project "addressclass"
    kind "ConsoleApp"
    language "C++"
    staticruntime "on"
    cppdialect "C++17"

    targetdir("%{wks.location}/bin/%{cfg.buildcfg}")
    objdir("%{wks.location}/obj/%{cfg.buildcfg}")

    files {
        "src/**.h",
        "src/**.hpp",
        "src/**.cpp",
    }

    includedirs {
        "src",
        NETCOM_INCLUDE
    }

    links {
    }

    filter "configurations:debug"
        runtime "Debug"
        symbols "On"

    filter "configurations:release"
        runtime "Release"
        optimize "On"
