#include <iostream>
#include <thread>
#include <chrono>
#include <cstdint>
#include <string>
#include <vector>
#include <csignal>

#include "netcom.h"

void onInterrupt(int signalNum) {
    std::exit(signalNum);
}

void onExit() {
    std::cout << netcom::RESET
              << netcom::SHOW
              << std::endl;
}

void handlePossible(std::map<IPv4::Part, uint32_t> const& cidrAddress, const uint32_t& bits) {
    using namespace std::chrono_literals;
    const auto mask = cidrAddress.at(IPv4::Part::Mask);

    std::this_thread::sleep_for(250ms);
    std::cout << "  Possible networks \n";
    std::cout << "  -------------------------------------\n";
    std::cout << "  Added: " << bits << " bits\n\n";
    std::cout << "               ";
    for (int i = 0; i < 32 + 3; i++) {
        std::cout << " ";
    }
    std::cout << "\n";
    std::cout << "  Subnet Mask: ";
    std::cout << IPv4::FormatBin(mask);
    std::stringstream ss;
    ss << "\u001b[" << 32 + 4 << "D";
    std::cout << "\u001b[1A" << ss.str();
    uint32_t ones = IPv4::GetOnes(mask) - bits;
    for (int i = 0, counter = 0; i < 32 + 3; i++) {
        if (i <= ones + 3)
            std::cout << "\u001b[2D" << "  ";
        if (counter < ones + bits && i % 9 != 0) {
                counter++;
                std::cout << "↓";
        } else std::cout << " ";

        std::cout << std::flush;
        if(i <= ones)
            std::this_thread::sleep_for(25ms);
        else if (counter < ones + bits)
            std::this_thread::sleep_for(150ms);
    }
    std::cout << "\u001b[1B";
    std::cout << "\n" << std::flush;

    std::cout << "  Networks: \n";
    std::cout << "  -------------------------------------\n";

    const uint32_t base = 2;
    uint32_t networkCount = base;
    for(int i = 1; i < bits; i++)
        networkCount = networkCount * base;

    int32_t oneCount = IPv4::GetOnes(mask);
    int32_t oneLeft = IPv4::GetOnes(~mask);
    uint32_t maskedBefore = IPv4::GenMask(ones);
    for(int32_t i = 0; i < networkCount; i++) {
        uint32_t ip = cidrAddress.at(IPv4::Part::IP) & maskedBefore;
        ip |= i << oneLeft;

        std::cout << "  " << i + 1 << ". ";
        std::cout << IPv4::FormatDec(ip) << "/" << oneCount;

        std::cout << "\n";
        std::this_thread::sleep_for(125ms);
    }

}

constexpr auto USAGE = R"(
  Usage:    subnet [address] [bits]

  Options:
            [address]   CIDR format address
            [bits]      Bit used in mask as network count

  Example:  subnet 172.16.128.0/26 3

            Prints the 2^3 network addresses.
            It uses 3 bit of the 26 ones that are
            available in the mask.
)";

auto main(int argc, char const* argv[]) -> int {
    // std::cout << netcom::CLEAR << netcom::HIDE << std::flush;
    std::cout << netcom::HIDE << std::flush;
    std::signal(SIGINT, onInterrupt);
    std::atexit(onExit);
    std::vector<std::string> args(argv, argv + argc);

    std::string address = "172.16.128.0/26";
    uint32_t    bits    = 3;

    if (argc == 3) {
        address = argv[1];
        try {
            bits = std::stoi(argv[2]);
        } catch (std::exception const& e) {
            std::cerr << netcom::RED << e.what() << netcom::RESET << '\n';
            bits = 3;
        }
    }

    if (argc < 3) {
        std::cout << USAGE;
        std::exit(0);
    }

    if (!IPv4::IsCIDRFormat(address)) {
        std::cerr << netcom::RED << "Address is not in CIDR format\n" << netcom::RESET;
        std::exit(0);
    }

    using namespace std::chrono_literals;

    std::cout << "                         \n";
    std::cout << "  IP Address Subnet Mask \n";
    std::cout << "  -----------------------\n";
    std::this_thread::sleep_for(125ms);

    std::cout << "  Input:  " << address << '\n';
    std::this_thread::sleep_for(125ms);
    std::cout << "  Format: " << "CIDR"  << '\n';
    std::this_thread::sleep_for(125ms);
    std::cout << "                           \n";
    std::this_thread::sleep_for(500ms);

    auto addressCIDR = IPv4::SplitCIDR(address);
    std::this_thread::sleep_for(125ms);
    std::cout << "  IP:     " << IPv4::FormatDec(addressCIDR[IPv4::Part::IP]) << '\n';
    std::this_thread::sleep_for(125ms);
    std::cout << "  Mask:   " << IPv4::FormatDec(addressCIDR[IPv4::Part::Mask]) << '\n';
    std::this_thread::sleep_for(125ms);
    std::cout << '\n';
    std::this_thread::sleep_for(500ms);

    const auto totalNetworks = (2 << (bits - 1));
    const auto hostPerNetworks = (~addressCIDR[IPv4::Part::Mask] - 1);
    std::cout << "  Networks:      " << totalNetworks   << '\n';
    std::this_thread::sleep_for(125ms);
    std::cout << "  Hosts/Network: " << hostPerNetworks << '\n';
    std::this_thread::sleep_for(125ms);
    std::cout << "  Total Hosts:   " << (totalNetworks * hostPerNetworks) << '\n';
    std::cout << '\n';
    std::this_thread::sleep_for(500ms);

    std::cout << "  Binary Format \n";
    std::cout << "  --------------\n";
    std::cout << "  IP:   " << IPv4::FormatBin(addressCIDR[IPv4::Part::IP])   << '\n';
    std::cout << "  Mask: " << IPv4::FormatBin(addressCIDR[IPv4::Part::Mask]) << '\n';
    std::cout << '\n';
    std::this_thread::sleep_for(500ms);

    handlePossible(addressCIDR, bits);

    return 0;
}

