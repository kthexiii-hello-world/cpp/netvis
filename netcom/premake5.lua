NETCOM_INCLUDE = path.getabsolute("include")

project "netcom"
    language "C++"
    cppdialect "C++17"
    kind "Utility"

    defines {
    }

    files {
        "include/**.h",
        "include/**.hpp",
    }

    includedirs {
        "include",
    }

    links {}

    filter "system:macosx"
        system "macosx"
        systemversion "latest"

    filter "system:linux"
        system "linux"
        systemversion "latest"

    filter "system:windows"
        system "windows"
        systemversion "latest"

