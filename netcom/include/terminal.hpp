#pragma once

namespace netcom {

constexpr auto CLEAR    = "\033[H\033[2J";
constexpr auto HIDE     = "\033[?25l";
constexpr auto SHOW     = "\033[?25h";
constexpr auto RESET    = "\u001b[0m";

constexpr auto ESCAPE   = "\u001b[";
constexpr auto UP       = 'A';
constexpr auto DOWN     = 'B';
constexpr auto LEFT     = 'D';
constexpr auto RIGHT    = 'C';

constexpr auto UP_1     = "\u001b[1A";
constexpr auto DOWN_1   = "\u001b[1B";
constexpr auto LEFT_1   = "\u001b[1D";
constexpr auto RIGHT_1  = "\u001b[1C";

constexpr auto PAD_TOP  = '\n';
constexpr auto PAD_LEFT = "  ";

constexpr auto RED       = "\u001b[31m";
constexpr auto BOLD      = "\u001b[1m";
constexpr auto UNDERLINE = "\u001b[4m";

}
