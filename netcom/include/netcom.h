#include <string>
#include <array>
#include <bitset>
#include <cstdint>
#include <sstream>
#include <map>

#include "terminal.hpp"

namespace IPv4 {

enum class Part : uint32_t {
    IP, Mask
};

uint32_t TextToU32(std::string ipv4text) {
    constexpr auto DELIMITER = '.';
    std::array<uint8_t, 4> numbers;
    int32_t pos = 0;

    for (int32_t i = 0; i < 4; i++) {
        pos = ipv4text.find(DELIMITER);
        if (pos != std::string::npos) {
            numbers[i] = std::stoi(ipv4text.substr(0, pos));
            ipv4text.erase(0, pos + 1);
        } else {
            numbers[i] = std::stoi(ipv4text.substr(0, ipv4text.length()));
        }
    }

    uint32_t ip = 0x00000000;
    ip |= ((numbers[0] << 24) & 0xFF000000);
    ip |= ((numbers[1] << 16) & 0x00FF0000);
    ip |= ((numbers[2] << 8)  & 0x0000FF00);
    ip |= ((numbers[3] << 0)  & 0x000000FF);

    return ip;
}

std::string FormatDec(const uint32_t& ipv4) {
    std::stringstream ss;
    ss << ((ipv4 & 0xFF000000) >> 24) << '.';
    ss << ((ipv4 & 0x00FF0000) >> 16) << '.';
    ss << ((ipv4 & 0x0000FF00) >>  8) << '.';
    ss << ((ipv4 & 0x000000FF) >>  0);
    return ss.str();
}

std::string FormatBin(const uint32_t& ipv4) {
    std::stringstream ss;
    ss << std::bitset<8>((ipv4 & 0xFF000000) >> 24) << '.';
    ss << std::bitset<8>((ipv4 & 0x00FF0000) >> 16) << '.';
    ss << std::bitset<8>((ipv4 & 0x0000FF00) >> 8)  << '.';
    ss << std::bitset<8>((ipv4 & 0x000000FF) >> 0);
    return ss.str();
}

std::string FormatHex(const uint32_t& ipv4) {
    std::stringstream ss;
    ss << std::hex << std::uppercase;
    ss << ((ipv4 & 0xFF000000) >> 24) << '.';
    ss << ((ipv4 & 0x00FF0000) >> 16) << '.';
    ss << ((ipv4 & 0x0000FF00) >>  8) << '.';
    ss << ((ipv4 & 0x000000FF) >>  0);
    return ss.str();
}

uint32_t GenMask(const int& ones, const bool& state = 1) {
    uint32_t mask = 0x00000000;
    for(int i = 0; i < ones; i++)
        mask |= 1 << i;
    mask <<= (32 - ones);
    return mask;
}

std::map<Part, uint32_t> SplitCIDR(const std::string& cidr) {
    std::string address = cidr;
    std::map<Part, uint32_t> ipParts;
    int32_t pos = address.find('/');
    uint32_t mask = std::stoi(address.substr(pos + 1, address.length()));
    mask = GenMask(mask);

    ipParts.insert(std::pair<Part, uint32_t>(Part::Mask, mask));

    address.erase(pos, address.length());
    ipParts.insert(std::pair<Part, uint32_t>(Part::IP, TextToU32(address)));
    return ipParts;
}

int32_t GetOnes(uint32_t data) {
    int32_t count = 0;
    for (int32_t i = 0; i < 32; i++) {
        count += data & 1;
        data >>= 1;
    }
    return count;
}

bool IsValidIP(const std::string& ip) {
    std::string address = ip;
    int32_t pos = 0;
    int32_t dotCount = 0;
    while((pos = address.find('.')) != std::string::npos && dotCount < 4) {
        dotCount++;
        address.erase(0, pos + 1);
    }
    return dotCount == 3;
}

bool IsCIDRFormat(const std::string& cidr) {
    if (cidr.find('/') == std::string::npos) return false;
    return IsValidIP(cidr);
}

} // namespace IPv4

